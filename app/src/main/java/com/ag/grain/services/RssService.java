package com.ag.grain.services;


import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.os.Bundle;
import android.os.IBinder;
import android.os.ResultReceiver;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.ag.grain.api.API;
import com.ag.grain.api.ApiService;
import com.ag.grain.helpers.DbHelper;
import com.ag.grain.models.RssFeed;
import com.ag.grain.models.RssFeedItem;

import java.io.Serializable;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class RssService extends Service {

    public static final String ITEMS = "items";
    public static final String RECEIVER = "receiver";
    public static final String BUNDLE = "bundle";
    private final static String TAG = "RssService";
    private final static int UPDATE_INTERVAL_MIN = 1;
    private List<RssFeedItem> feedItemListList = null;
    private API api;
    private DbHelper dbHelper;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        Log.d(TAG, "Bound to service");
        return null;
    }

    @Override
    public int onStartCommand(final Intent intent, int flags, int startId) {

        Log.d(TAG, "Service started");

        dbHelper = new DbHelper(this);

        api = ApiService.getApi();

        // обновляем ленту
        updateRssFeedItems();

        AlarmManager alarm = (AlarmManager) getSystemService(ALARM_SERVICE);
        alarm.set(
                AlarmManager.RTC_WAKEUP,
                System.currentTimeMillis() + (1000 * 60 * UPDATE_INTERVAL_MIN),
                PendingIntent.getService(getApplicationContext(), 0, new Intent(this, RssService.class), 0)
        );
        return Service.START_STICKY;
    }

    private void updateRssFeedItems() {

        Observable<RssFeed> getRssFeedObservable = api.getRssFeed()
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());

        getRssFeedObservable
                .subscribe(new Observer<RssFeed>() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {

                    }

                    @Override
                    public void onNext(RssFeed rssFeed) {
                        feedItemListList = rssFeed.getChannel().getItemList();

                        // при успешном обновлении данных отправляем данные через broadcast в активити и сохраняем в базе
                        sendDataToActivity();
                        storeFeedItems();
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        Log.d(TAG, e.getLocalizedMessage());

                        // при не успешном обновлении данных тянем их из базы и отправляем через broadcast в активити
                        feedItemListList = dbHelper.getFeedFromDB();
                        sendDataToActivity();
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    private void storeFeedItems() {

        dbHelper.insertFeedList(feedItemListList);

    }

    private void sendDataToActivity() {

        if (feedItemListList != null) {
            Intent intent = new Intent(RECEIVER);
            Bundle bundle = new Bundle();
            bundle.putSerializable(ITEMS, (Serializable) feedItemListList);
            intent.putExtra(BUNDLE, bundle);
            LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(intent);
        }
    }
}
