package com.ag.grain;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Handler;
import android.os.ResultReceiver;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import com.ag.grain.adapters.RssFeedAdapter;
import com.ag.grain.api.API;
import com.ag.grain.api.ApiService;
import com.ag.grain.helpers.DbHelper;
import com.ag.grain.models.RssFeed;
import com.ag.grain.models.RssFeedItem;
import com.ag.grain.services.RssService;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class MainActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener {

    @BindView(R.id.feed_list) RecyclerView mFeedListView;
    @BindView(R.id.swipe_container) SwipeRefreshLayout mSwipeContainer;

    private API api;
    private DbHelper dbHelper;

    private RssFeedAdapter mAdapter;
    private List<RssFeedItem> mRssFeedItems = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        api = ApiService.getApi();
        dbHelper = new DbHelper(this);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        mFeedListView.setLayoutManager(linearLayoutManager);

        mSwipeContainer.setOnRefreshListener(this);

        mAdapter = new RssFeedAdapter(mRssFeedItems);
        mFeedListView.setAdapter(mAdapter);

        startService(); //запускаем сервис обновления фида
    }

    @Override
    protected void onResume() {
        super.onResume();

        // регистрируем слушатель новых данных для ленты
        LocalBroadcastManager.getInstance(this).registerReceiver(mMessageReceiver, new IntentFilter(RssService.RECEIVER));

    }

    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Bundle bundle = intent.getBundleExtra(RssService.BUNDLE);
            List<RssFeedItem> items = (List<RssFeedItem>) bundle.getSerializable(RssService.ITEMS);
            if (items != null) {
                Log.d("Rss", "new data");
                mRssFeedItems.clear();
                mRssFeedItems.addAll(items);
                mAdapter.notifyDataSetChanged();
            }


        }
    };

    private void startService() {
        Intent intent = new Intent(getApplicationContext(), RssService.class);
        getApplicationContext().startService(intent);
    }

    // получение новых данных при свайпе
    private void refreshFeed() {

        mSwipeContainer.setRefreshing(true);

        Observable<RssFeed> getRssFeedObservable = api.getRssFeed()
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());

        getRssFeedObservable
                .subscribe(new Observer<RssFeed>() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {

                    }

                    @Override
                    public void onNext(RssFeed rssFeed) {

                        mSwipeContainer.setRefreshing(false);

                        List<RssFeedItem> rssFeedItemList = rssFeed.getChannel().getItemList();

                        mRssFeedItems.clear();
                        mRssFeedItems.addAll(rssFeedItemList);
                        mAdapter.notifyDataSetChanged();

                        storeFeedItems(rssFeedItemList); // сохраним данные в базу
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        mSwipeContainer.setRefreshing(false);
                    }

                    @Override
                    public void onComplete() {

                    }
                });

    }

    @Override
    public void onRefresh() {
        refreshFeed();
    }

    private void storeFeedItems(List<RssFeedItem> rssFeedItemList) {

        dbHelper.insertFeedList(rssFeedItemList);

    }
}
