package com.ag.grain;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.ag.grain.models.RssFeedItem;
import com.bumptech.glide.Glide;

import butterknife.BindView;
import butterknife.ButterKnife;

public class FeedItemActivity extends AppCompatActivity {

    public static final String EXTRA_ITEM = "extra_item";

    @BindView(R.id.image) ImageView mImage;
    @BindView(R.id.title) TextView mTitle;
    @BindView(R.id.description) TextView mDescription;
    @BindView(R.id.category) TextView mCategory;
    @BindView(R.id.date) TextView mDate;
    @BindView(R.id.link) TextView mLink;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feed_item);
        ButterKnife.bind(this);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

        final RssFeedItem mItem = (RssFeedItem) getIntent().getSerializableExtra(EXTRA_ITEM);

        Glide.with(this)
                .load(mItem.getEnclosure().getUrl())
                .into(mImage);

        mTitle.setText(mItem.getTitle());
        mDescription.setText(mItem.getDescription());
        mCategory.setText(mItem.getCategory());
        mDate.setText(mItem.getPublicationDate());
        mLink.setText(mItem.getLink());

        if (mItem.getLink() != null) {
            mLink.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(mItem.getLink()));
                    startActivity(browserIntent);
                }
            });
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        if (id == android.R.id.home) {
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
