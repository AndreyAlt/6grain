package com.ag.grain.models;


import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

import java.io.Serializable;
import java.util.List;

@Root(strict = false)
public class RssChannel implements Serializable {

    @ElementList(name = "item", required = true, inline = true)
    private List<RssFeedItem> itemList;

    public List<RssFeedItem> getItemList() {
        return itemList;
    }
}
