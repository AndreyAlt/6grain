package com.ag.grain.models;

import android.provider.BaseColumns;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

import java.io.Serializable;

@Root(name = "item", strict = false)
public class RssFeedItem implements Serializable {

    @Element(name = "title", required = true )
    private String title;

    @Element(name = "pubDate", required = true )
    private String publicationDate;

    @Element(name = "description", required = true )
    private  String description;

    @Element(name = "category", required = true )
    private  String category;

    @Element(name = "link", required = true )
    private  String link;

    @Element(name = "enclosure", required = false)
    private Enclosure enclosure;

    public String getTitle() {
        return title;
    }

    public String getPublicationDate() {
        return publicationDate;
    }

    public String getDescription() {
        return description;
    }

    public String getCategory() {
        return category;
    }

    public String getLink() {
        return link;
    }

    public Enclosure getEnclosure() {
        return enclosure;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setPublicationDate(String publicationDate) {
        this.publicationDate = publicationDate;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public void setEnclosure(Enclosure enclosure) {
        this.enclosure = enclosure;
    }

    public static final class FeedItemEntry implements BaseColumns {

        public final static String TABLE_NAME = "feed";

        public final static String _ID = BaseColumns._ID;
        public final static String COLUMN_TITLE = "title";
        public final static String COLUMN_DESC = "description";
        public final static String COLUMN_LINK = "link";
        public final static String COLUMN_DATE = "pubDate";
        public final static String COLUMN_CATEGORY = "category";
        public final static String COLUMN_IMAGE_URL = "image_url";
    }
}
