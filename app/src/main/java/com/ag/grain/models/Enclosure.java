package com.ag.grain.models;


import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Root;

import java.io.Serializable;


public class Enclosure implements Serializable {

    @Attribute(name = "url")
    private String url;

    @Attribute(name = "length")
    private long length;

    @Attribute(name = "type")
    private String type;

    public Enclosure() {
    }

    public Enclosure(String url) {
        this.url = url;
    }

    public String getUrl() {
        return url;
    }

    public long getLength() {
        return length;
    }

    public String getType() {
        return type;
    }
}