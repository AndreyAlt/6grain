package com.ag.grain.helpers;


import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.provider.BaseColumns;

import com.ag.grain.models.Enclosure;
import com.ag.grain.models.RssFeedItem;

import java.util.ArrayList;
import java.util.List;

import static com.ag.grain.models.RssFeedItem.FeedItemEntry;

public class DbHelper extends SQLiteOpenHelper implements BaseColumns {

    public static final String NAME = "rss_feed.db";
    private static final int VERSION = 1;

    public DbHelper(Context context) {
        super(context, NAME, null, VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        String createFeedTable = "CREATE TABLE " + FeedItemEntry.TABLE_NAME + " (" +
                FeedItemEntry._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                FeedItemEntry.COLUMN_TITLE + " VARCHAR(255) NOT NULL, " +
                FeedItemEntry.COLUMN_DESC + " TEXT NOT NULL, " +
                FeedItemEntry.COLUMN_LINK + " VARCHAR(255) NOT NULL, " +
                FeedItemEntry.COLUMN_DATE + " VARCHAR(100) NOT NULL, " +
                FeedItemEntry.COLUMN_CATEGORY + " VARCHAR(50) NOT NULL, " +
                FeedItemEntry.COLUMN_IMAGE_URL + " VARCHAR(255)" +
                ");";

        db.execSQL(createFeedTable);

        insertDefaultData(db);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + FeedItemEntry.TABLE_NAME);
        onCreate(db);
    }

    private void insertFeedItem(RssFeedItem feedItem, SQLiteDatabase db) {
        ContentValues values = new ContentValues();
        values.put(FeedItemEntry.COLUMN_TITLE, feedItem.getTitle());
        values.put(FeedItemEntry.COLUMN_DESC, feedItem.getDescription());
        values.put(FeedItemEntry.COLUMN_CATEGORY, feedItem.getCategory());
        values.put(FeedItemEntry.COLUMN_DATE, feedItem.getPublicationDate());
        values.put(FeedItemEntry.COLUMN_LINK, feedItem.getLink());

        String imageUrl = feedItem.getEnclosure() == null ? "" : feedItem.getEnclosure().getUrl();

        values.put(FeedItemEntry.COLUMN_IMAGE_URL, imageUrl);

        db.insert(FeedItemEntry.TABLE_NAME, null, values);
    }

    public void clearTable() {
        SQLiteDatabase db = getWritableDatabase();
        db.delete(FeedItemEntry.TABLE_NAME, null, null);
    }

    public void insertFeedItem(RssFeedItem feedItem) {
        SQLiteDatabase db = getWritableDatabase();

        insertFeedItem(feedItem, db);
    }

    public List<RssFeedItem> getFeedFromDB() {
        SQLiteDatabase db = getReadableDatabase();

        String[] projection = {
                FeedItemEntry.COLUMN_TITLE,
                FeedItemEntry.COLUMN_DESC,
                FeedItemEntry.COLUMN_LINK,
                FeedItemEntry.COLUMN_CATEGORY,
                FeedItemEntry.COLUMN_DATE,
                FeedItemEntry.COLUMN_IMAGE_URL
        };

        Cursor cursor = db.query(
                FeedItemEntry.TABLE_NAME,
                projection,
                null,
                null,
                null,
                null,
                null);

        List<RssFeedItem> rssFeedItemList = new ArrayList<>();

        while (cursor.moveToNext()) {
            RssFeedItem rssFeedItem = new RssFeedItem();

            int titleColumnIndex = cursor.getColumnIndex(FeedItemEntry.COLUMN_TITLE);
            int descColumnIndex = cursor.getColumnIndex(FeedItemEntry.COLUMN_DESC);
            int linkColumnIndex = cursor.getColumnIndex(FeedItemEntry.COLUMN_LINK);
            int categoryColumnIndex = cursor.getColumnIndex(FeedItemEntry.COLUMN_CATEGORY);
            int dateColumnIndex = cursor.getColumnIndex(FeedItemEntry.COLUMN_DATE);
            int imageUrlColumnIndex = cursor.getColumnIndex(FeedItemEntry.COLUMN_IMAGE_URL);

            rssFeedItem.setTitle(cursor.getString(titleColumnIndex));
            rssFeedItem.setDescription(cursor.getString(descColumnIndex));
            rssFeedItem.setLink(cursor.getString(linkColumnIndex));
            rssFeedItem.setCategory(cursor.getString(categoryColumnIndex));
            rssFeedItem.setPublicationDate(cursor.getString(dateColumnIndex));

            Enclosure enclosure = new Enclosure(cursor.getString(imageUrlColumnIndex));

            rssFeedItem.setEnclosure(enclosure);

            rssFeedItemList.add(rssFeedItem);
        }

        cursor.close();

        return rssFeedItemList;
    }

    public void insertFeedList(List<RssFeedItem> feedItemListList) {

        clearTable();

        for (RssFeedItem rssFeedItem : feedItemListList) {
            insertFeedItem(rssFeedItem);
        }

    }

    private void insertDefaultData(SQLiteDatabase db) {
        RssFeedItem rssFeedItem = new RssFeedItem();

        rssFeedItem.setTitle("Василий Уткин упал со стула в прямом эфире");
        rssFeedItem.setDescription("Футбольный комментатор Василий Уткин упал со стула во время эфира на «Спорт FM». «Это примерно пятнадцатый конь, павший подо мной, начиная с восьмого класса школы. Но первый — в эфире!» — пошутил журналист. Несколько лет назад вес Уткина составлял 230 килограммов, но ему удалось похудеть.");
        rssFeedItem.setLink("https://lenta.ru/news/2017/11/02/utkin/");
        rssFeedItem.setCategory("Интернет и СМИ");
        rssFeedItem.setPublicationDate("Thu, 02 Nov 2017 20:28:00 +0300");
        rssFeedItem.setEnclosure(new Enclosure("https://icdn.lenta.ru/images/2017/11/02/20/20171102202842548/pic_b35444be0ad1c8e34cf1712eb48cc814.jpg"));

        insertFeedItem(rssFeedItem, db);

        rssFeedItem.setTitle("В Киеве обвинили поляков в варварстве");
        rssFeedItem.setDescription("Глава украинского Института национальной памяти Владимир Вятрович заявил, что в Польше варварски относятся к украинским памятникам. Так представитель Киева прокомментировал заявление члена польского сената Яна Жарина, который обвинил Украину в осквернении польских мемориалов.");
        rssFeedItem.setLink("https://lenta.ru/news/2017/11/02/vyatrovich/");
        rssFeedItem.setCategory("Бывший СССР");
        rssFeedItem.setPublicationDate("Thu, 02 Nov 2017 20:07:00 +0300");
        rssFeedItem.setEnclosure(new Enclosure("https://icdn.lenta.ru/images/2017/11/02/19/20171102195530630/pic_2eae3df19bea22c9872788c07df1f425.jpg"));

        insertFeedItem(rssFeedItem, db);

        rssFeedItem.setTitle("Овечкин соберет «команду Путина»");
        rssFeedItem.setDescription("Российский хоккеист Александр Овечкин объявил о создании общественного движения под названием Putin Team («команда Путина»). «Быть частью такой команды — для меня гордость, это похоже на ощущение, когда ты надеваешь майку сборной России, зная, что за тебя болеет вся страна», — написал спортсмен в Instagram.");
        rssFeedItem.setLink("https://lenta.ru/news/2017/11/02/putinteam/");
        rssFeedItem.setCategory("Спорт");
        rssFeedItem.setPublicationDate("Thu, 02 Nov 2017 19:55:00 +0300");
        rssFeedItem.setEnclosure(new Enclosure("https://icdn.lenta.ru/images/2017/11/02/19/20171102195002924/pic_7f66a12d28130326a8c918d706905ac1.jpg"));

        insertFeedItem(rssFeedItem, db);
    }
}
