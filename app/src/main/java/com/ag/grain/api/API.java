package com.ag.grain.api;

import com.ag.grain.models.RssFeed;

import io.reactivex.Observable;
import retrofit2.http.GET;

public interface API {

    @GET("news")
    Observable<RssFeed> getRssFeed();

}
