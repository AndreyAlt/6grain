package com.ag.grain.api;


import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.simplexml.SimpleXmlConverterFactory;

public class ApiService {

    private static final String RSS_LINK = "https://lenta.ru/rss/";

    private static API sApi;

    public static API getApi() {
        if (sApi == null) {
            HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
            logging.setLevel(HttpLoggingInterceptor.Level.BODY);

            OkHttpClient.Builder httpClient = new OkHttpClient.Builder()
                    .readTimeout(60, TimeUnit.SECONDS)
                    .writeTimeout(60, TimeUnit.SECONDS)
                    .connectTimeout(60, TimeUnit.SECONDS);
            httpClient.addInterceptor(logging);

            Retrofit retrofit = new Retrofit.Builder()
                    .addConverterFactory(SimpleXmlConverterFactory.createNonStrict())
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .baseUrl(RSS_LINK)
                    .client(httpClient.build())
                    .build();

            sApi = retrofit.create(API.class);
        }
        return sApi;
    }

}
