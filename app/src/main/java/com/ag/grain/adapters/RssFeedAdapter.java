package com.ag.grain.adapters;


import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.ag.grain.FeedItemActivity;
import com.ag.grain.R;
import com.ag.grain.models.RssFeedItem;
import com.bumptech.glide.Glide;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.ag.grain.FeedItemActivity.EXTRA_ITEM;
import static com.bumptech.glide.request.RequestOptions.centerCropTransform;

public class RssFeedAdapter extends RecyclerView.Adapter<RssFeedAdapter.ViewHolder> {

    private List<RssFeedItem> listData;

    public RssFeedAdapter(List<RssFeedItem> dataset) {
        listData = dataset;
    }

    @Override
    public RssFeedAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_feed, parent, false);

        ViewHolder vh = new ViewHolder(v);

        return vh;
    }

    @Override
    public void onBindViewHolder(RssFeedAdapter.ViewHolder holder, int position) {
        holder.setItem(listData.get(position));
    }

    @Override
    public int getItemCount() {
        return listData.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        @BindView(R.id.image) ImageView mImage;
        @BindView(R.id.title) TextView mTitle;
        @BindView(R.id.description) TextView mDescription;

        private RssFeedItem mItem;

        private View mRootView;
        private Context mContext;

        public ViewHolder(View v) {
            super(v);
            v.setOnClickListener(this);
            ButterKnife.bind(this, v);
            mRootView = v;
            mContext = v.getContext();

        }

        public void setItem(RssFeedItem item) {
            mItem = item;

            mTitle.setText(mItem.getTitle());
            mDescription.setText(mItem.getDescription().trim());

            if (mItem.getEnclosure() != null) {
                mImage.setVisibility(View.VISIBLE);
                Glide.with(mContext)
                        .load(mItem.getEnclosure().getUrl())
                        .apply(centerCropTransform())
                        .into(mImage);
            } else {
                mImage.setVisibility(View.GONE);
            }

        }

        @Override
        public void onClick(View v) {
            Intent intent = new Intent(mContext, FeedItemActivity.class);
            intent.putExtra(EXTRA_ITEM, mItem);
            mContext.startActivity(intent);
        }
    }

}
